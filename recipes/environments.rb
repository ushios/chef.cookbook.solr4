#
# Cookbook Name:: solr4
# Recipe:: environments
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

template '/etc/profile.d/solr4.sh' do
	source 'etc/profile.d/solr4.sh.erb'
	owner 'root'
	group 'root'
	mode 0644
end

