#
# Cookbook Name:: solr4
# Recipe:: default
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'solr4::environments'
include_recipe 'solr4::install_java'
include_recipe 'solr4::install'
include_recipe 'solr4::config'
include_recipe 'solr4::daemon'