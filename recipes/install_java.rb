#
# Cookbook Name:: solr4
# Recipe:: install_java
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

package 'openjdk-6-jdk' do
	action :install
end