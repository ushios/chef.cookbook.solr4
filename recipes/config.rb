#
# Cookbook Name:: solr4
# Recipe:: daemon
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

directory '/etc/solr4' do
	action :create
	recursive true
	owner 'root'
	group 'root'
	mode 0755
end

template '/etc/solr4/solr4.conf' do
	source 'etc/solr4/solr4.conf.erb'
	mode 0644
	owner 'root'
	group 'root'
	variables({
		:port => node[:solr4][:port],
		:host => node[:solr4][:host],
		:zkHost => node[:solr4][:zkHost],
		:maxMemory => node[:solr4][:maxMemory],
		:minMemory => node[:solr4][:minMemory]
	})
end
