#
# Cookbook Name:: solr4
# Recipe:: install
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

remote_file "/usr/local/#{node['solr4']['tar_filename']}.tgz" do
	source node['solr4']['tar_url']
	action :create_if_missing
end

execute 'untar solr4' do
	command "tar xzf #{node['solr4']['tar_filename']}.tgz"
	cwd '/usr/local'
	creates "/usr/local/#{node['solr4']['tar_filename']}"
	user 'root'
	action :run
end

link '/usr/local/solr4' do
	to "/usr/local/#{node['solr4']['tar_filename']}"
	owner 'root'
	group 'root'
end