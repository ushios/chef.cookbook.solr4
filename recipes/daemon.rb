#
# Cookbook Name:: solr4
# Recipe:: daemon
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

package 'daemon' do
	action :install
end

directory '/var/log/solr4' do
	action :create
	recursive true
	owner 'root'
	group 'root'
	mode 0755
end

template '/etc/logrotate.d/solr4' do
	source 'etc/logrotate.d/solr4.erb'
	mode 0644
	owner 'root'
	group 'root'
end

template '/etc/init.d/solr4' do
	source 'etc/init.d/solr4.erb'
	mode 0755
	owner 'root'
	group 'root'
end

execute 'update-rc.d solr4 defaults' do
	command 'update-rc.d solr4 defaults && touch /usr/local/solr4/.update-rc.d'
	creates '/usr/local/solr4/.update-rc.d'
	user 'root'
	group 'root'
end